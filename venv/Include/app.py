import speech_recognition as sr
import os
import sys
import webbrowser
import pyttsx3

def talk(words):
    print(words)
    engine = pyttsx3.init()
    engine.say(words)
    engine.runAndWait()

talk("Привет, спроси у меня что-либо")

def command():
    r = sr.Recognizer()

    with sr.Microphone() as source:
        print("Говорите")
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)

    try:
         zadanie = r.recognize_google(audio, language="ru-RU").lower()
         print("Вы сказали " +  zadanie)
    except sr.UnknownValueError:
         talk("Я вас не поняла")
         zadanie = command()

    return zadanie

def makeSomething(zadanie):
    if 'открыть сайт' in zadanie:
        talk("Уже открываю")
        url = 'https://google.ru'
        webbrowser.open(url)
    elif 'стоп' in zadanie:
        talk("Да, конечно")
        sys.exit()
    elif 'имя' in zadanie:
        talk ("Меня зовут Алиса")

while True:
    makeSomething(command())